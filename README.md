## پروژه جبرخطی 

### کاربرد جبر خطی در نرم افزار اندرویدی که دمای N نقطه را با استفاده از دیتا فیوژن به دست آورد 

استاد: دکتر میگلی

حل تمرین: غلامرضا قاسمی

تهیه کننده: علی قنبری

## فایل ها

برنامه اندروید: [دانلود](/uploads/687f3f25db9410f3c8df97b2cdd1e616/lak-2.apk)

گزارش کتبی:[دانلود](https://gitlab.com/ali-ghanbari/lak/-/raw/master/reports/ali%20ghanbari%20-%20report.docx)

سرس پروژه: [دانلود](https://gitlab.com/ali-ghanbari/lak/-/archive/master/lak-master.zip)


#### build command
`flutter build apk --target-platform android-arm`